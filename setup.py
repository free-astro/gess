import setuptools
import versioneer

FULLVERSION = versioneer.get_version()
print(FULLVERSION)

if __name__ == "__main__":
    setuptools.setup(version=versioneer.get_version(),
        cmdclass=versioneer.get_cmdclass())