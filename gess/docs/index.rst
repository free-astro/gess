.. GeSS documentation master file, created by
   sphinx-quickstart on Tue Jan 26 22:46:29 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


Welcome to GeSS !
=================

GeSS (Generate Siril Scripts) is a package built upon `pySiril <https://gitlab.com/free-astro/pysiril>`_, a Python wrapper for `Siril <https://www.siril.org/>`_ astronomical processing software.

Its main purpose is to batch-process astrophotography sessions, somewhat like using Siril scripts or `SiriLic <https://gitlab.com/free-astro/sirilic>`_, with following features:

- gess can be used to generate on-the-fly new scripts by passing keywords or ticking a few boxes to modify your usual processing workflow,

- gess can name Siril sequences based on info contained in your images headers, so that you resulting stacks have meaningful names,

- gess can locate automatically masters from libraries based on the values in the FITS header of frames to be calibrated,

- gess can crawl all your folders from the night before and capture anything that needs to be calibrated, registered and stacked,

- gess can build all your master libraries if the clouds rolled in and you decided not to loose the night,

- gess can gather calibrated lights from multiple sessions, and then register/stack them.


Gess is based on fetching info from image headers and parsing them to recognize string patterns in your files and folders names. As such, it is primarily intended for users with a repeatable acquisition process. Most likely using an imaging software that nicely arrange frames in folders with a stable naming convention.
Please also have a look at the section regarding :ref:`folders conventions<Important: Folders and files>`.


Many options/preferences on how you want to process your files can be specified through configuration files. A disclaimer here: No fancy GUI except for settings! GeSS is intended for users who have some kind of drive towards automation and command line. It you do not recognize yourself in these words, then probably GeSS will be more of a burden than a help.


Knowing Python is not necessary, though it can prove useful if you want to re-use some modules of the package to build your own scripts. After installing the package, just type *gess* in a shell to get some help on how to use it (or much better, keep on reading these pages just a little bit longer).

Which module should you use?
----------------------------

+ Single sessions

   - If you want to process a single imaging session, you should have a look at using GeSS in :ref:`interactive <Interactive mode>` or :ref:`command-line <Command line mode>` modes. Probably a good idea to follow the steps shown in the :ref:`Jump Start<Jump Start>` section if this is your first use.

   - After you have processed a few sessions on the same target, you will want to gather and process them with the :ref:`multisession <Multisession mode>` mode.


+ Batch processing 

   - If you need to batch process multiple sessions from the same night, with multiple targets, filters or build master libraries, you need to head to :ref:`loop mode <Loop mode>` section.

   - Once multiple sessions are batch-processed, you can reorganize the calibrated lights using  :ref:`copyback <Copyback mode>` utility.


.. toctree::
   :maxdepth: 3
   :hidden:

   BeforeYouStart.rst
   JumpStart.rst
   FirstSteps.rst
   AdvancedUse.rst
   Options.rst
   FilesandFolders.rst
   Examples.rst
   PythonReference.rst




.. Indices and tables
.. ------------------

.. * :ref:`genindex`
.. * :ref:`modindex`
.. * :ref:`search`
