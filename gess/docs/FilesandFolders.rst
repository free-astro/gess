Files and Folders naming
========================

.. |br| raw:: html

    <br>

.. |brl| raw:: latex

        \mbox{}\newline

GeSS uses different names for sequences so that you can see at a glance which pre- and processing steps have been applied.

Preprocessed files
++++++++++++++++++

After preprocessing, your calibrated lights \(and sequence\) should be named like this:

pp\(d\)\(f\)\(deb\)\_\(lights\)\_

- \(d\) is for darks substracted.
- \(f\) is for flats applied.
- \(deb\) is for debayering applied.
- \(lights\) is the name of the subfolder into which the lights are stored or the name specified by :ref:`lightsfmt<lightsfmt>` if you have specified one.

Examples:

- *ppdfdeb\_light\_.seq* is the name of a sequence with both darks/flats preprocessing and debayering applied.
- *ppddeb\_light\_.seq* is same as above except you have asked to skip preprocessing with flats.


Processed files
+++++++++++++++

As names of calibrated sequences are passed on to the next steps \(background extraction, registration, stacking\), you can have different versions of processing on the same base sequence that coexist in the same folder.

At the end at the processing, result files are named as follows, again depending on the processing steps you have chosen to perform:

\(r\_\)\(layer\_\)\(bkg\_\)\(preprocessedlight\)\_stacked.\(ext\)

- \(r\_\) is for global resgistration applied.
- \(layer\_\) is either Ha or OIII if layer extraction is applied \(color shots only\).
- \(bkg\_\) is for background extrcation \(linear gradient removed\) applied.
- \(preprocessedlights\) is the string reflecting your preprocessing steps, see above.
- \(ext\) is the extension of your FITS files.

Examples:

*r\_Ha\_bkg\_ppdf\_light\_stacked.fits* is the stacked output of a lights sequence, preprocessed with dark and flats, with Ha layer and background, after being registered and stacked.


Storage folders
+++++++++++++++

All along the process, gess will create the following folders to store intermediate and results files:

- masters: where all the masters are stored, whether thay have been copied from libraries or stacked on the spot. Have a look at :ref:`copymasters<copymasters>` if you want to store a hard copy of masters coming from libraries.
- process\(bd\): storing intermediate files. `bd` is either 16 or 32 depending on chosen bitdepth.
- results\(layer\)\(bd\): storing the stacked files. `layer` is none for normal processing and HaOIII is you have asked for Ha/OIII layers extraction.

Once you are happy with the single or multiple processings of your session, you can discard the `process` folder. |br| |brl|

If you have also discarded the original calibration folders (darks, flats etc...) to save some space but have kept the `masters` folder, it will act as a local master library if you come back later and want to process again the same session.






