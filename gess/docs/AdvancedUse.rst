.. |br| raw:: html

    <br>

.. |brl| raw:: latex

        \mbox{}\newline


Advanced Use
============

This section presents two more advanced modes. It is recommended to understand how gessengine works from the First Steps :ref:`section <Processing worflow of an imaging session>` before using them.


Loop mode
---------

Loop mode is intended for more advanced users. You will probably need to have processed sucessfully a few individual sessions before jumping to this usage.

.. note::

    Important: Before trying to run GeSS in loop mode, you need to have set :ref:`loop default options<Setting loop.cfg>`.

Loop crawls your imaging folder and process all the FOLDERS (not files) with a specified string in their path. By default, it will search for the date of the day before but you can also specify any string to search for (a previous date, an object name etc...).

Loop will do the following:

- First identify all the folders containing calibration frames (darks, biases, flats) and light frames,

- Prepare masterframes if required in that order: biases, flats, darks

- Store them in your libraries (if you are working with libraries for some or all masters),

- Process all the light frames folders.


With this mode, you can therefore assemble all your masters libraries, process multiple sessions with multiple targets, filters, exposures etc... with one single line of command.

In NINA (and most probably in other imaging software), you can specify this as the script to be executed at the end of the session. You wake up the next morning to a handfull of processed images to review with your morning cup of coffee.

To run loop, from a shell:

.. code-block:: 

    gess -l
    gess -l gess_setup2.cfg
    gess -l gess_setup2.cfg loop_setup2.cfg
    gess -l searchstr=M31


From Python:

.. code-block:: python

    from gess import gessloop

    # success,res=gessloop.Run(addgesscfg='',addloopcfg='',searchstr='')

    success,res=gessloop.Run()
    success,res=gessloop.Run('gess_setup2.cfg')
    success,res=gessloop.Run('gess_setup2.cfg','loop_setup2.cfg')
    success,res=gessloop.Run(searchstr='M31')

- **gess -l** runs gessloop using default options from gess.cfg and loop.cfg, in the folders containing the date of the day before.

- **gess -l gess_setup2.cfg** runs gessloop using default options from gess.cfg with addtional options from *gess_setup2.cfg*.

- **gess -l gess_setup2.cfg loop_setup2.cfg** runs gessloop using default options from gess.cfg with addtional options from *gess_setup2.cfg* and loop.cfg updated from *loop_setup2.cfg*.

- **gess -l searchstr=M31** runs gessloop using default options from gess.cfg and loop.cfg, in the folders containing the string *M31* (so not necessarily a date).

Loop is set to go on through the different folders it should process and run till the end. If, along the way, some folder processing fails, it will show in the summary and go on with processing the next folder. So please carefully review the summary shown at the end of execution to make sure everything went as expected.


Copyback mode
-------------

 
.. note::

    Important: Before trying to run copyback mode, you need to have set :ref:`copyback default options<Setting copyback.cfg>`.


This mode can be used to gather calibrated lights from multiple sessions in an automated way. Once you are done imaging a target, all the calibrated lights are ready for final registration and stacking.

It is intended for users imaging sessions on the same targets overmultiple nights, with mutliple filters ans most importantly with a repeatable acquisition process. If you want to gather sessions occasionally, you are better off using :ref:`Multisession mode`.

To run copyback, from a shell:

.. code-block:: 

    gess -c
    gess -c gess_setup2.cfg
    gess -c gess_setup2.cfg loop_setup2.cfg 2021-01-02 copyback_setup2.cfg
    gess -c searchstr=M31


From Python:

.. code-block:: python

    from gess import gesscopyback

    # success,res=copyback.Run(addgesscfg='',addloopcfg='',searchstr='',addcopybackcfg='')

    success,res=gesscopyback.Run()
    success,res=gesscopyback.Run('gess_setup2.cfg')
    success,res=gesscopyback.Run('gess_setup2.cfg','loop_setup2.cfg','2021-01-02','copyback_setup2.cfg')
    success,res=gesscopyback.Run(searchstr='M31')

- **gess -c** runs copyback using default options from gess.cfg, loop.cfg and copyback.cfg, in the folders containing the date of the day before.

- **gess -c gess_setup2.cfg** runs copyback using default options from gess.cfg with addtional options from *gess_setup2.cfg*.

- **gess -c gess_setup2.cfg loop_setup2.cfg 2021-01-02 copyback_setup2.cfg** runs copyback using default options from gess.cfg, loop.cfg and copyback.cfg, complemented with options in *gess_setup2.cfg*, *loop_setup2.cfg* and *copyback_setup2.cfg*. It searches the folders containing string "2021-01-02".

- **gess -c searchstr=M31** runs copyback using default options from gess.cfg, loop.cfg and copyback.cfg, in the folders containing the string *M31* (so not necessarily a date).

.. note::

    Before copying a calibrated light in the destination folder, copyback will check if a file with the same *DATE-OBS* FITS header key is present to avoid copying twice (or more) a file. It will also deal with incrementing the sequential number to avoid copying over an existing file.