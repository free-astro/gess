Before you start
================
.. _astropy: https://www.astropy.org/
.. |astropy| replace:: **astropy**
.. _pySimpleGUI: https://pysimplegui.readthedocs.io/en/latest/
.. |pySimpleGUI| replace:: **pySimpleGUI**
.. _Siril: https://siril.org/download/
.. |Siril| replace:: **Siril**
.. _pySiril: https://gitlab.com/free-astro/pysiril
.. |pySiril| replace:: **pySiril**
.. _Python:  https://www.python.org/downloads/
.. |Python| replace:: **Python**
.. _pyexiv2:  https://pypi.org/project/pyexiv2/
.. |pyexiv2| replace:: **pyexiv2**


.. |br| raw:: html

    <br>

.. |brl| raw:: latex

        \mbox{}\newline

Prerequisites
-------------

Before installing GeSS, you will need to install:

-  a recent version of |Python|_, 3.6 or above. Please note that versions 2.x are not supported.

- the latest version of |Siril|_ (1.2.0) or above.

.. warning::

   **For Windows users**: Python and its scripts folder need to be in your PATH to be able to use GeSS. Remember to enable this when installing Python for Windows (`Add Python to your PATH`). And please please, install Python for all users when prompted to do so, to avoid any installation in obscure hidden folders only Windows would know of.

The following Python packages will be installed/updated if not already present:

- |pySiril|_ (>=0.0.17)
  
- |pySimpleGUI|_ (<= 4.60.5)

- |astropy|_

- |pyexiv2|_

.. warning::

   **For Windows users**: enable symbolic links by switching Windows to developper mode: head to Windows 10 Settings > Update & Security > For Developers and select “Developer mode”.


Installation
------------

If you want the latest release version:

- download the wheel from `release page <https://gitlab.com/cissou8/gess/-/releases>`_

- in a shell, type:

.. code-block:: 

   pip install gess-x.y.z-py3-none-any.whl


x.y.z being the version number of the release.


If you want to build from the sources using pip (requires `setuptools <https://pypi.org/project/setuptools/>`_ and `wheel <https://pypi.org/project/wheel/>`_ packages):

.. code-block:: 

   pip install https://gitlab.com/free-astro/gess/-/archive/maindev/gess-master.tar.gz       # stable version
   pip install https://gitlab.com/free-astro/gess/-/archive/maindev/gess-maindev.tar.gz      # dev version


Important: Folders and files
----------------------------

GeSS is built around the same philosophy as Siril standard scripts. Consequently, it follows the same convention. An imaging session consists of a working directory (as you would set the home directory in Siril), containing at least a subfolder with lights. It can also contain other subfolders with darks, flats and/or biases. 
The name you choose for all these subfolders is up to you (or the imaging software you use) and can be configured through options. But this filing architecture is mandatory for GeSS to work.

Example with NINA as imaging software:

A valid filename specifier could be |br| |brl|
$$TARGETNAME$$\\$$DATEMINUS12$$\\\ **$$IMAGETYPE$$**\\$$IMAGETYPE$$_$$FRAMENR$$ |br| |brl|
returning a file tree like this:  

.. code-block::

   MyAstroPics
   └── M31
      ├── 2021-01-01
      │   ├── FLAT
      │   │   ├── FLAT_0001.FITS
      │   │   └── FLAT_0002.FITS
      │   └── LIGHT
      │       ├── LIGHT_0001.FITS
      │       └── LIGHT_0002.FITS
      └── 2021-01-02

The **LIGHT** folder containing the light frames is a subfolder to MyAstroPics\\M31\\2021-01-01 which is the working directory.


On the contrary, using this string specifier produces a non-valid file tree:|br| |brl|
$$TARGETNAME$$\\\ **$$IMAGETYPE$$**\\$$DATEMINUS12$$\\$$IMAGETYPE$$_$$FRAMENR$$

.. code-block::

   MyAstroPics
   └── M31
      ├── FLAT
      │   ├── 2021-01-01
      │   │   ├── FLAT_0001.FITS
      │   │   └── FLAT_0002.FITS
      │   └── 2021-01-02
      └── LIGHT
         ├── 2021-01-01
         │   ├── LIGHT_0001.FITS
         │   └── LIGHT_0002.FITS
         └── 2021-01-02

The **LIGHT** folder does not contain the light frames. There are located further down the tree inside the 2021-01-01 subfolder.

If you are already used to working with Siril, this should not be any news to you. And if you are new to Siril and want to unleash the power of batch processing with GeSS, then, you will need to comply with this convention...



