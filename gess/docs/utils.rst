.. GeSS documentation master file, created by
   sphinx-quickstart on Tue Jan 26 22:46:29 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

gess.utils
==========


SSFy
++++

.. automodule:: gess.utils.SSFy
   :members: Run


CFGy
++++

.. automodule:: gess.utils.CFGy
   :members: Run
 







