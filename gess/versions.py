from . import _version
__version__ = _version.get_versions()['version']
__sirilminversion__ = "1.2.0"