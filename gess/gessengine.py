""" 
    This module is the core module of GeSS, called by most other modules. It takes as input a working directory path (similar to the working directory for launching a script in Siril) and a dictionnary specifying the options you want to apply.

"""

import os,sys
from pysiril.siril import Siril
from pysiril.wrapper import Wrapper
from pysiril.addons import Addons
import time
import glob

if __package__ is None or len(__package__)==0: # for debugging purposes only
    sys.path.append(".")

from gess.common.Logger import Logger
from gess.common.helpers import checkcalibexists,checklocalcalibexists,checksubfolder,ReadSirilPrefs,getfirstfile,parsemasterformat,GetHeader
from gess.common.options import options
from gess.common.i18n import _


from gess.versions import __version__, __sirilminversion__
VERSION = __version__

def cleanclose(app,saveout,opt,needtoclose,message,success=False):
    res={}
    res['message']=message
    print(message)
    if not success:
        print(_('Aborting'))
    if needtoclose:
        app.Close()
        del app
    if opt.debug:
        sys.stdout = saveout
    return success,res

def cleanseqandfitseq(seqname):
    v=[seqname+'.seq']
    v+=[seqname+'_.seq']
    v+=[seqname+'.fits']
    v+=[seqname+'_.fits']
    for f in v:
        if os.path.isfile(f):
            os.remove(f)


# main engine
def Run(workdir,opt=None,app=None,dryrun=False):   
    """ 
    :param workdir: the path to the working directory.
    :type workdir: str

    :param opt: a DictX instance containing all the options for the processing workflow described in the :ref:`First Steps<First Steps>` section, defaults to None. Can be formed as the options member of an :ref:`Options<Options>` instance, with optiontype='gess'. Have a look at :ref:`gess.cfg settings<Setting gess.cfg>` for a full description of all the options keys.
    :type opt: DictX, optional

    :param app: a Siril class instance that can be used to call pySiril functions.
                Pass None if it needs to be started, defaults to None.
    :type app: Siril, optional

    :param dryrun: flag to stop engine just after starting Siril, if set to True. Used mainly for checking that the options are passed correctly and that the masters can be found, defaults to False.
    :type dryrun: bool, optional

    :return: (True if successful, dict with keys specifying inputs and outputs)
    :rtype: (bool,dict)

            The bool is set to True is the processing was successful.

            The dictionnary contains the following keys (if successful):

            - 'dark': the masterdark that was used (if any)

            - 'flat': the masterflat that was used (if any)

            - 'offset': the masteroffset that was used (if any)

            - 'workdir': the path to the working directory

            - 'options': the options which were passed

            - 'version': gess version number

            - 'log': the path to the session log file if debug was on

            Otherwise, if something wrong happened, the dictionnary contains a single key 'messsage' detailing where the process failed.
    """    

    needtoclose=True
    res={} 
    os.chdir(workdir)
    saveout = sys.stdout
    res['log']=''
    if (not opt is None) and (len(opt)==0):
        res={}
        res['message']=_('cfg:Could not load the cfg file')
        print(res['message'])
        return False,res
    if opt.debug:
        sys.stdout = Logger()
        res['log']=os.path.join(workdir,sys.stdout.dbgfile)
    startt = time.time()
    if opt is None:
        opt=options(optiontype='gess').getoptions()
    originaloptions=opt.copy()

    msg='* gess v'+VERSION+' *'
    print('*'*len(msg))
    print(msg)
    print('*'*len(msg))
    print('')
    print('gess options')

    for k,v in opt.items():
        print(k+' : '+str(v))
    print('')    

    needtoclose=False
    if app is None:
        print(_('Starting PySiril'))
        needtoclose=True
        if len(opt.sirilexe)==0:
            app=Siril(bStable=False,requires=__sirilminversion__)
        else:
            app=Siril(opt.sirilexe,bStable=False,requires=__sirilminversion__)
        print('')

    #preparing intermediate storage folders

    process_dirname='process'+str(opt.bitdepth) 
    process_dir = '../'+process_dirname
    process_dir_path=os.path.realpath(os.path.join(workdir,process_dirname))

    if not os.path.isdir(process_dir_path): # v0.5.2 - preparing process folder 
        os.mkdir(process_dir_path)

    masters_dirname='masters'
    masters_dir = '../'+masters_dirname
    masters_dir_path=os.path.realpath(os.path.join(workdir,masters_dirname))   


    if opt.pplight:
        # first check that lights folder exists
        success,_dump=checksubfolder(workdir,opt.lights)
        if not success:
            return cleanclose(app,saveout,opt,needtoclose,_('cfg:lights - Could not find folder containing lights'))
    #checking lights sequence naming
    if (len(opt.lightsfmt)>0) & opt.pplight:
        firstlight=getfirstfile(os.path.join(workdir,opt.lights))
        hdr=GetHeader(firstlight)
        lightseqname=parsemasterformat(opt.lightsfmt,hdr,firstlight)
        if lightseqname=='':
            return cleanclose(app,saveout,opt,needtoclose,_('cfg:lightsfmt - Could not set the lights sequence name'))
    else:
        lightseqname=opt.lights

    # raw option to use convertraw instead of convert to avoid capturing jpg
    israw=False
    if 'raw' in opt.ext.lower():
        opt.ext='fit' # then .fit it is
        israw=True

    # check if there is an autoHO specification and figure out if doHO is True/False
    opt.autoHOspec=opt.autoHOspec.strip()
    if (len(opt.autoHOspec)>0) & ('=' in opt.autoHOspec) & (opt.pplight):
        firstlight=getfirstfile(os.path.join(workdir,opt.lights))
        hdr=GetHeader(firstlight)
        k,v=opt.autoHOspec.split('=',1)
        if ((k in hdr.keys()) & (hdr[k].strip().lower()==v.strip().lower())):
            opt.doHO=True
        else:
            opt.doHO=False

    # If ppdark, need to check that we have either:
    # - a darks library containing a masterdark matching the lights or
    # - a darks subfolder to build the master from

    if opt.ppdark: # check if local master is available - check if dark library or subfolder + check if matching masterdark exists
        success=False
        if os.path.isdir(masters_dir_path):
            success,masterdark=checklocalcalibexists('dark',masters_dir_path,opt.darksfmt,opt.lights,opt.ext,app)
        if success:
            hasdarklib=True
        else:
            success,hasdarklib,masterdark=checkcalibexists('dark',opt.darks,opt.darksfmt,workdir,opt.lights,opt.ext,app)
            if not success:
                return cleanclose(app,saveout,opt,needtoclose,_('cfg:darks/darksfmt - Could not find darks'))

    # If ppflat, need to check that we have either:
    # - a flats master or
    # - a bias library containing a masterbias matching the flats or
    # - a bias subfolder to build the master from

    hasflatlib=False #in case this is called by loop to create masteroffset

    if opt.ppflat: # check if local master is available - check if offset library or subfolder + check if matching masteroffset exists
        success=False
        if os.path.isdir(masters_dir_path):
            success,masterflat=checklocalcalibexists('flat',masters_dir_path,opt.flatsfmt,opt.lights,opt.ext,app)
        if success:
            hasflatlib=True
        else:
            success,hasflatlib,masterflat=checkcalibexists('flat',opt.flats,opt.flatsfmt,workdir,opt.lights,opt.ext,app)
            if not success:
                return cleanclose(app,saveout,opt,needtoclose,_('cfg:flats/flatsfmt - Could not find flats'))


    if opt.ppoffset:
        if not hasflatlib: # if the masterflat does not exists, need to check that we have offsets to calibrate #v0.5.2 removed
            hasoffsetlib=False
            hassynthoffset=False
            hasbias=False
            success=False
            if os.path.isdir(masters_dir_path): #just in case the masterflat was deleted but not the master offset...quite unlikely but why not
                success,masterbias=checklocalcalibexists('bias',masters_dir_path,opt.biasesfmt,opt.flats,opt.ext,app)
            if success:
                hasbiaslib=True
            else:
                success,hasbiaslib,masterbias=checkcalibexists('bias',opt.biases,opt.biasesfmt,workdir,opt.flats,opt.ext,app)
            if success:
                hasbias=True
            if hasbiaslib:
                hasoffsetlib=True
            else:
                if len(masterbias)>0:
                    hassynthoffset=True
                    masterbias='"'+masterbias+'"' #adding quotes
            if (not hasbias):
                return cleanclose(app,saveout,opt,needtoclose,_('cfg:biases/biasesfmt - Could not find any subfolder or library containing biases'))
        
    # Now that the calibration frames are all sorted out, we are ready to process
 
    if opt.dostack:  # Need to create the results folder as stack command does not create new folders like convert does
        results_dirname='results'+str(opt.bitdepth) 
        res_dir='../'+results_dirname
        res_dir_path=os.path.realpath(os.path.join(workdir,results_dirname))
        if not os.path.isdir(res_dir_path):
            os.mkdir(res_dir_path)

    #creating masters folder if it doesn't exist
    if not os.path.isdir(masters_dir_path): # v0.6.0 - preparing masters folder to copy masters
        os.mkdir(masters_dir_path) 

    if not(app.bOpened):   
        print(' ')
        print(_('Starting Siril'))   
        app.Open() # to avoid reopen and start outside


    app.tr.Configure(False,False,True,True) #Muting pySiril info and log 
    cmd=Wrapper(app)    
    AO=Addons(app)

    print(' ')
    # Applying Siril options
    
    success=eval('cmd.set{:d}bits()'.format(opt.bitdepth)) # Not nice but found no other way as bitdepth is not parsed
    if not success:
        return cleanclose(app,saveout,opt,needtoclose,_('Could not set bitdepth to {:d} bits').format(opt.bitdepth))
    
    success=cmd.setext(opt.ext)
    if not success:
        return cleanclose(app,saveout,opt,needtoclose,_('Could not set extension to {:s}').format(opt.ext))

    #v0.5.2 added compression option if not null in opt:
    if len(opt.compression)>0:
        success=cmd.Execute("setcompress "+opt.compression)
        if not success:
            return cleanclose(app,saveout,opt,needtoclose,_('setcompress command failed with arguments {:s}').format(opt.compression))

    success=cmd.cd(workdir) # in case it's called several time wo Open to the correct dir
    if not success:
        return cleanclose(app,saveout,opt,needtoclose,_('Could not set current directory to {:s}').format(workdir))
    
    if dryrun:
        return cleanclose(app,saveout,opt,needtoclose,_('Dry-run successful'),success=True)


    refflat=''
    refoffset=''
    refdark=''
    if opt.ppoffset:
        if not hasflatlib:
            if not (hasoffsetlib|hassynthoffset): # if no library or synthoffset level, we need to stack the offsets
                refoffset=os.path.join(masters_dir_path,'offset_stacked.'+opt.ext)
                if not os.path.isfile(refoffset):
                    print('Stacking offsets')
                    success=cmd.cd(opt.biases)
                    if not success:
                        return cleanclose(app,saveout,opt,needtoclose,_('Could not set current directory to {:s}').format(opt.biases))
                    cleanseqandfitseq(os.path.join(process_dir_path,'offset'))
                    if israw:
                        success=cmd.convertraw('offset', out=process_dir, fitseq=opt.seqasfitseq,start=1)
                        if not success:
                            return cleanclose(app,saveout,opt,needtoclose,_('Convertraw command failed for {:s}').format('offsets'))
                    else:
                        success=cmd.convert('offset', out=process_dir, fitseq=opt.seqasfitseq,start=1)
                        if not success:
                            return cleanclose(app,saveout,opt,needtoclose,_('Convert command failed for {:s}').format('offsets'))
                    success=cmd.cd(process_dir)
                    if not success:
                        return cleanclose(app,saveout,opt,needtoclose,_('Could not set current directory to {:s}').format(process_dir))

                    success=cmd.stack('offset', type='rej', sigma_low=3, sigma_high=3, norm='no')
                    if not success:
                        return cleanclose(app,saveout,opt,needtoclose,_('Stack command failed for {:s}').format('offsets'))
                    #0.6.0 hardcopy to masters folder
                    AO.CopyLink(os.path.join(process_dir_path,'offset_stacked.'+opt.ext),refoffset, bCopyMode=True)
                    cmd.cd('..')
                else: 
                    print(_('Master offset already stacked - skipping'))
            else:
                refoffset=masterbias
                # v 0.6.0 - Copy master to processfolder to avoid any relative path - link or hardcopy depending on options
                if not hassynthoffset:
                    if not os.path.isfile(os.path.join(masters_dir_path,os.path.split(refoffset)[1].replace(' ',''))):
                        AO.CopyLink(refoffset, os.path.join(masters_dir_path,os.path.split(refoffset)[1].replace(' ','')), bCopyMode=opt.copymasters) #removing blanks in symcopy of masters
            if not hassynthoffset:
                refoffset=os.path.split(refoffset)[1].replace(' ','') 
                refoffset=os.path.join(masters_dir,refoffset)


    if opt.ppflat: #preparing masterflat
        if not hasflatlib:
            # Preprocessing and stacking flats
            refflat=os.path.join(masters_dir_path,'pp_flat_stacked.'+opt.ext)
            if not os.path.isfile(refflat): 
                success=cmd.cd(opt.flats)
                if not success:
                    return cleanclose(app,saveout,opt,needtoclose,_('Could not set current directory to {:s}').format(opt.flats))
                cleanseqandfitseq(os.path.join(process_dir_path,'flat'))
                if israw:
                    success=cmd.convertraw('flat', out=process_dir, fitseq=opt.seqasfitseq,start=1 )
                    if not success:
                        return cleanclose(app,saveout,opt,needtoclose,_('Convertraw command failed for {:s}').format('flats'))
                else:
                    success=cmd.convert('flat', out=process_dir, fitseq=opt.seqasfitseq,start=1 )
                    if not success:
                        return cleanclose(app,saveout,opt,needtoclose,_('Convert command failed for {:s}').format('flats'))
                print(_('Preprocessing flats'))
                cmd.cd(process_dir )
                cleanseqandfitseq(os.path.join(process_dir_path,'pp_flat'))
                success=cmd.calibrate('flat', bias=refoffset )
                if not success:
                    return cleanclose(app,saveout,opt,needtoclose,_('Preprocess command failed for {:s}').format('flats'))
                print(_('Stacking flats'))
                success=cmd.stack('pp_flat', type='rej', sigma_low=3, sigma_high=3, norm='mul')
                if not success:
                    return cleanclose(app,saveout,opt,needtoclose,_('Stack command failed for {:s}').format('flats'))
                #0.6.0 hardcopy to masters folder
                AO.CopyLink(os.path.join(process_dir_path,'pp_flat_stacked.'+opt.ext),refflat, bCopyMode=True)
                cmd.cd('..')
            else: #V0.1
                print(_('Master flat already stacked - skipping'))   
        else:
            refflat=masterflat
            # v 0.6.0 - Copy master to processfolder to avoid any relative path - link or hardcopy depending on options
            if not os.path.isfile(os.path.join(masters_dir_path,os.path.split(refflat)[1].replace(' ',''))):
                AO.CopyLink(refflat, os.path.join(masters_dir_path,os.path.split(refflat)[1].replace(' ','')), bCopyMode=opt.copymasters) #removing blanks in symcopy of masters
        refflat=os.path.split(refflat)[1].replace(' ','')  
        refflat=os.path.join(masters_dir,refflat)

    if opt.ppdark : #preparing masterdark
        if not hasdarklib:
            refdark=os.path.join(masters_dir_path,'dark_stacked.'+opt.ext)
            if not os.path.isfile(refdark): 
                print(_('Stacking darks'))
                success=cmd.cd(opt.darks)
                if not success:
                    return cleanclose(app,saveout,opt,needtoclose,_('Could not set current directory to {:s}').format(opt.darks))
                cleanseqandfitseq(os.path.join(process_dir_path,'dark'))
                if israw:
                    success=cmd.convertraw('dark', out=process_dir, fitseq=opt.seqasfitseq,start=1) 
                    if not success:
                        return cleanclose(app,saveout,opt,needtoclose,_('Convertraw command failed for {:s}').format('darks'))
                else:
                    success=cmd.convert('dark', out=process_dir, fitseq=opt.seqasfitseq,start=1)
                    if not success:
                        return cleanclose(app,saveout,opt,needtoclose,_('Convert command failed for {:s}').format('darks'))
                cmd.cd(process_dir)
                success=cmd.stack('dark', type='rej', sigma_low=3, sigma_high=3, norm='no')
                if not success:
                    return cleanclose(app,saveout,opt,needtoclose,_('Stack command failed for {:s}').format('darks'))
                #0.6.0 hardcopy to masters folder
                AO.CopyLink(os.path.join(process_dir_path,'dark_stacked.'+opt.ext),refdark, bCopyMode=True)
                cmd.cd('..')

            else:
                print(_('Master dark already stacked - skipping'))
        else:
            refdark=masterdark
            # v 0.6.0 - Copy master to processfolder to avoid any relative path - link or hardcopy depending on options
            if not os.path.isfile(os.path.join(masters_dir_path,os.path.split(refdark)[1].replace(' ',''))):
                AO.CopyLink(refdark, os.path.join(masters_dir_path,os.path.split(refdark)[1].replace(' ','')), bCopyMode=opt.copymasters) #removing blanks in symcopy of masters
        refdark=os.path.split(refdark)[1].replace(' ','') 
        refdark=os.path.join(masters_dir,refdark)


    prefix=''
    if opt.pplight: #calibrate light frames
        print(_('Preprocessing lights'))
        success=cmd.cd(opt.lights)
        if not success:
            return cleanclose(app,saveout,opt,needtoclose,_('Could not set current directory to {:s}').format(opt.lights))
        # check if converted lights sequence already exists
        # if true skip conversion
        lightseq=glob.glob(os.path.join(process_dir_path,lightseqname+'*.seq'))
        if len(lightseq)==0:
            cleanseqandfitseq(os.path.join(process_dir_path,lightseqname))
            if israw:
                success=cmd.convertraw(lightseqname, out=process_dir, fitseq=opt.seqasfitseq,start=1)
                if not success:
                    return cleanclose(app,saveout,opt,needtoclose,_('Convertraw command failed for {:s}').format('lights'))
            else:
                success=cmd.convert(lightseqname, out=process_dir, fitseq=opt.seqasfitseq,start=1)
                if not success:
                    return cleanclose(app,saveout,opt,needtoclose,_('Convert command failed for {:s}').format('lights'))  
        else:
            print(_('lights already converted - skipping'))
        cmd.cd(process_dir)

        # building keywords dictionnary for calibrate command
        kw={}
        prefix='pp'
        if opt.ppdark:
            kw['dark']=refdark
            prefix+='d'
        if opt.ppflat:
            prefix+='f'
            kw['flat']=refflat
        if 'cfa' in opt.shottype.lower():
            kw['cfa']=True
            kw['equalize_cfa']=True
            if opt.doHO: 
                kw['debayer']=False
            else:
                kw['debayer']=True
                prefix+='deb'
        prefix+='_'
        kw['prefix']=prefix

        if prefix=='pp_': # case if no ppflat, nor dark and w/o debayer (doHO or mono), we don't need to calibrate
            prefix=''
        else:
            pplightseq=glob.glob(os.path.join(process_dir_path,prefix+lightseqname+'*.seq'))
            if len(pplightseq)==0:
                cleanseqandfitseq(os.path.join(process_dir_path,prefix+lightseqname))
                success=cmd.calibrate(lightseqname, **kw)
                if not success:
                    return cleanclose(app,saveout,opt,needtoclose,_('Preprocess command failed for {:s}').format('lights'))  
            else:
                print(_('lights already preprocessed - skipping'))

    else: # required if not pp_light but need to go on with other options
        cmd.cd(process_dirname)

    if opt.doHO: 
        prefix1='Ha_'+prefix
        prefix2='OIII_'+prefix
        if 'Ha' in lightseqname: 
            lightseqname=lightseqname.split('_',1)[1]
        else:
            #v0.5.2 to deal with bkg option later on
            cleanseqandfitseq(os.path.join(process_dir_path,prefix1+lightseqname))
            cleanseqandfitseq(os.path.join(process_dir_path,prefix2+lightseqname))
            success=cmd.seqextract_HaOIII(prefix+lightseqname)
            if not success:
                return cleanclose(app,saveout,opt,needtoclose,_('seqextract_HaOIII command failed for sequence {:s}').format(prefix+lightseqname))  


    #v0.5.2 - added background extraction option before registration
    if opt.dobkg:
        if not opt.doHO:
            cleanseqandfitseq(os.path.join(process_dir_path,'bkg_'+prefix+lightseqname))
            success=cmd.seqsubsky(prefix+lightseqname,1)
            if not success:
                return cleanclose(app,saveout,opt,needtoclose,_('background extraction command failed for sequence {:s}').format(prefix+lightseqname)) 
            prefix='bkg_'+prefix
        else:
            cleanseqandfitseq(os.path.join(process_dir_path,'bkg_'+prefix1+lightseqname))
            success=cmd.seqsubsky(prefix1+lightseqname,1)
            if not success:
                return cleanclose(app,saveout,opt,needtoclose,_('background extraction command failed for sequence {:s}').format(prefix1+lightseqname)) 
            prefix1='bkg_'+prefix1
            cleanseqandfitseq(os.path.join(process_dir_path,'bkg_'+prefix2+lightseqname))
            success=cmd.seqsubsky(prefix2+lightseqname,1)
            if not success:
                return cleanclose(app,saveout,opt,needtoclose,_('background extraction command failed for sequence {:s}').format(prefix2+lightseqname)) 
            prefix2='bkg_'+prefix2
        


    if opt.doregister:
        if not opt.doHO: 
            cleanseqandfitseq(os.path.join(process_dir_path,'r_'+prefix+lightseqname))
            success=cmd.register(prefix+lightseqname)
            if not success:
                return cleanclose(app,saveout,opt,needtoclose,_('register command failed for sequence {:s}').format(prefix+lightseqname))
            prefix='r_'+prefix
        else:
            cleanseqandfitseq(os.path.join(process_dir_path,'r_'+prefix1+lightseqname))
            success=cmd.register(prefix1+lightseqname,drizzle=True)
            if not success:
                return cleanclose(app,saveout,opt,needtoclose,_('register command failed for sequence {:s}').format(prefix1+lightseqname))
            prefix1='r_'+prefix1    
            cleanseqandfitseq(os.path.join(process_dir_path,'r_'+prefix2+lightseqname))
            success=cmd.register(prefix2+lightseqname,drizzle=True)
            if not success:
                return cleanclose(app,saveout,opt,needtoclose,_('register command failed for sequence {:s}').format(prefix2+lightseqname))
            prefix2='r_'+prefix2             


    
    if opt.dostack:
        if not opt.doHO: 
            success=cmd.stack(prefix+lightseqname, type='rej', sigma_low=3, sigma_high=3, norm='addscale', output_norm=True, out=res_dir+'/'+prefix+lightseqname+'_stacked')
            if not success:
                return cleanclose(app,saveout,opt,needtoclose,_('stack command failed for sequence {:s}').format(prefix+lightseqname))
        else:
            success=cmd.stack(prefix1+lightseqname, type='rej', sigma_low=3, sigma_high=3, norm='addscale', output_norm=True, out=res_dir+'/'+prefix1+lightseqname+'_stacked')
            if not success:
                return cleanclose(app,saveout,opt,needtoclose,_('stack command failed for sequence {:s}').format(prefix1+lightseqname))
            success=cmd.stack(prefix2+lightseqname, type='rej', sigma_low=3, sigma_high=3, norm='addscale', output_norm=True, out=res_dir+'/'+prefix2+lightseqname+'_stacked')
            if not success:
                 return cleanclose(app,saveout,opt,needtoclose,_('stack command failed for sequence {:s}').format(prefix2+lightseqname))
            cmd.cd(res_dir)
            cmd.load(prefix2+lightseqname+'_stacked')
            cmd.linear_match(prefix1+lightseqname+'_stacked',low=0.0,high=0.92)
            cmd.save(prefix2+lightseqname+'_stacked')

    cmd.cd('..')
    cmd.close()
    if needtoclose: # Closing if Open() was not triggered by an external caller
        app.Close()
        del app
    print('gess v'+VERSION+':end')

    res['dark']=refdark
    res['flat']=refflat
    res['offset']=refoffset
    res['workdir']=workdir
    res['options']=originaloptions #to preserve some options keys which may have been modified by the process
    res['version']=VERSION
    print('gess execution time: {:3.1f}s'.format(time.time() - startt))

    if opt.debug:
        sys.stdout = saveout
    return True,res


def main():
    addcfgfile=''
    optdict={}
    for a in sys.argv[2:]:
        if '=' in a:
            f,v=a.split('=',1)
            if not '.cfg' in v:
                optdict[f]=v
            else:
                addcfgfile=v
        else:
            addcfgfile=a

    opt=options(addcfgfile=addcfgfile,dictcfg=optdict).getoptions()

    Run(sys.argv[1],opt)

if __name__ == "__main__":

    addcfgfile=''
    optdict={}
    for a in sys.argv[2:]:
        if '=' in a:
            f,v=a.split('=',1)
            if not '.cfg' in v:
                optdict[f]=v
            else:
                addcfgfile=v
        else:
            addcfgfile=a

    opt=options(addcfgfile=addcfgfile,dictcfg=optdict).getoptions()

    Run(sys.argv[1],opt)





