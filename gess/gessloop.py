""" 
    This module is used to batch-process multiple sessions by calling iteratively :py:mod:`gess.gessengine`. More help on its usage can be found in :ref:`loop mode<loop mode>` section.

"""
import os,sys
from datetime import datetime,timedelta
from pysiril.siril import Siril
from pysiril.wrapper import Wrapper
from pysiril.addons import Addons
import shutil


if __package__ is None or len(__package__)==0: # for debugging purposes only
    sys.path.append(".")

from gess import gessengine

from gess.common.helpers import getfirstfile,fast_scandir,checkmastersconfiguration,checkmastersubs,ParseSirilBDC,masterstackingoptions,parsemasterformat,GetHeader
from gess.common.options import options
from gess.common.DictX import DictX
from gess.common.Logger import Logger
from gess.common.i18n import _

from gess.versions import __version__, __sirilminversion__
VERSION = __version__

def Run(addgesscfg='',addloopcfg='',searchstr=''):
    """ main function of gessloop.

    :param addgesscfg: the path to a gess-style additional cfg file, defaults to ''.
    :type addcfgfile: str, optional
    :param addloopcfg: the path to a loop-style additional cfg file, defaults to ''.
    :type addloopcfg: str, optional
    :param searchstr: the string to be searched for in your imaging folder, defaults to ''
    :type searchstr: str, optional

    :return: (True if successful, dict with 'message' key summarizing the different folders processed)
    :rtype: (bool,dict)
    """    

    out={}
    out['message']=[]

    def printout(message,out=out):
        out['message']+=[message]
        print(out['message'][-1])
        return out

    Loop=options(optiontype='loop',addcfgfile=addloopcfg)
    loop=Loop.getoptions()
    if len(loop)==0:
        out=printout(_('Could not load the optional loop configuration file - aborting'))
        return False,out
    Opt=options(optiontype='gess',addcfgfile=addgesscfg)
    opt=Opt.getoptions()
    if len(opt)==0:
        out=printout(_('Could not load the optional gess configuration file - aborting'))
        return False,out

    maindir=loop.imagingfolder
    os.chdir(maindir)

    if opt.debug:
        sys.stdout = Logger('gessloop_')
        out['log']=os.path.join(maindir,sys.stdout.dbgfile)
        opt.debug=False

    print(_('Starting pySiril'))
    if len(opt.sirilexe)==0:
        app=Siril(bStable=False,requires=__sirilminversion__)
    else:
        app=Siril(opt.sirilexe,bStable=False,requires=__sirilminversion__)
    print('')
    app.tr.Configure(False,False,True,True) #Muting pySiril info and log 


    def cleanclose(message,out=out,app=app,success=False):
        out['message']+=[message]
        if app.bOpened:
            app.Close()
        del app
        msg=_('* GeSS loop summary *')
        print('\n\n')
        print('*'*len(msg))
        print(msg)
        print('*'*len(msg))
        print('')

        for m in out['message']:
            print(m)
        return success,out

    if len(searchstr)==0: # if no date is passed, then we look for the date 12 hrs before
        searchdate=datetime.now()-timedelta(hours=12)
        searchstr=searchdate.strftime(loop.datefmt)

    copydarks2lib=False
    copyflats2lib=False
    copybiases2lib=False

    # Checking if some dofs need to be stacked and placed in libraries
    if opt.ppdark:
        success,copydarks2lib,darks,darkslib,_dump=checkmastersconfiguration('darks',opt,loop)
        if not success:
            return cleanclose(_('Aborting'))

    if opt.ppflat:
        success,copyflats2lib,flats,flatslib,_dump=checkmastersconfiguration('flats',opt,loop)
        if not success:
            return cleanclose(_('Aborting'))

        success,copybiases2lib,biases,biaseslib,_dump=checkmastersconfiguration('biases',opt,loop)
        if not success:
            return cleanclose(_('Aborting'))

    subbydate=[]

    out=printout(_('\nSearching for all folders under {0:s} containing {1:s}').format(maindir,searchstr))
    
    for root, dirs, _dump in os.walk(maindir):
        for d in dirs:
            if searchstr in d:
                subbydate.append(os.path.join(root,d))
                

    if len(subbydate)==0:
        return cleanclose(_('None found - Aborting'))
    else:
        subbydate=list(set(subbydate)) #removing duplicates
        for s in subbydate:
            out=printout(s)

        out=printout('')

    origsettings=ParseSirilBDC(app=app)
    needtochangeBDC=False


    if opt.ppdark:
        optdark=masterstackingoptions(opt,'dark',library=copydarks2lib)
        if copydarks2lib:
            darkssubs=checkmastersubs('darks',darks,opt,subbydate,app=app,follownaming=os.path.splitext(opt.darksfmt)[0])
            needtochangeBDC=True

    if opt.ppflat:
        optflat=masterstackingoptions(opt,'flat',library=copyflats2lib)
        if copyflats2lib:
            flatssubs=checkmastersubs('flats',flats,opt,subbydate,app=app,follownaming=os.path.splitext(opt.flatsfmt)[0])
            needtochangeBDC=True

    if opt.ppoffset:
        optoffset=masterstackingoptions(opt,'offset',library=copybiases2lib)
        if copybiases2lib:
            biasessubs=checkmastersubs('biases',biases,opt,subbydate,app=app,follownaming=os.path.splitext(opt.biasesfmt)[0])
            needtochangeBDC=True

    if len(opt.lightsfmt)==0:
        lightssubs=checkmastersubs('lights',opt.lights,opt,subbydate,app=app,findsubsets=False)
    else:
        lightssubs=checkmastersubs('lights',opt.lights,opt,subbydate,app=app,findsubsets=True,follownaming=opt.lightsfmt)

    # getting existing compression settings to reinstate original settings at the end
    # masters to be stored in libraries will be stacked in 16b uncompressed to ensure max compatibility

    out=printout(_('Starting Siril'))
    app.Open()
    AO=Addons(app)
    out=printout(_('Done\n'))


    def stackmasters4lib(subs,mastername,optmaster,masterlib,app,AO,out=out):
        for f in subs:
            out=printout(_('Stacking {0:s} from {1:s}').format(mastername,f))
            workdir,last=os.path.split(f)
            optmaster[mastername]=last # in case of subsets
            resref={'darks':'dark','flats':'flat','biases':'offset'}
            process_dirname='process'+str(optmaster.bitdepth)
            refframe=getfirstfile(f)
            hdr=GetHeader(refframe)
            newfilename=parsemasterformat(optmaster[mastername+'fmt'],hdr,refframe,mode='w')
            newname=os.path.normpath(os.path.join(masterlib,newfilename))
            safecopydir=os.path.join(masterlib,'previous')
            if not os.path.isdir(safecopydir):
                AO.MkDirs(safecopydir)
            if os.path.isfile(newname): #avoid restacking masters if already in library with date matching
                hdr=GetHeader(newname)
                firstmaster=getfirstfile(f)
                hdr1=GetHeader(firstmaster)
                if hdr['DATE-OBS'][0:19]==hdr1['DATE-OBS'][0:19]:
                    out=printout(_('{0:s} from {1:s} were already stacked and stored in your library - skipping').format(mastername,f))
                    out=printout('')
                    continue

            status,res=gessengine.Run(workdir,optmaster,app)
            if status:
                stacked=os.path.normpath(os.path.join(workdir,process_dirname,res[resref[mastername]]))
                if os.path.isfile(newname): # an older version is present in the library - need to make a copy in case we need to revert the change
                    savedate=datetime.now()
                    savedatestr=savedate.strftime('_'+loop.datefmt+'_%H%M%S')
                    previousversion=newname.replace(masterlib,safecopydir)
                    previousversion,ext=os.path.splitext(previousversion)
                    previousversion=previousversion+savedatestr+ext
                    AO.CopyLink(newname,previousversion,bCopyMode=True)
                    out=printout(_('Previous version of master{0:s} {1:s} has been safely copied to: {2:s}').format(mastername,newfilename,previousversion))
                out=printout(_('New version of master{0:s} {1:s} has been copied to library: {2:s}').format(mastername,newfilename,masterlib))
                AO.CopyLink(stacked,newname,bCopyMode=True)
                shutil.rmtree(os.path.join(workdir,process_dirname)) # remove process folder to avoid re-using the same frames if more than one master is processed
                os.remove(stacked) # remove master to avoid it being detected for subsequent masters stackings
            else:
                out=printout(res['message'])
                out=printout(_('{0:s} from {1:s} could not be stacked - check the log').format(mastername,f))
            out=printout('')
        return

    if opt.ppoffset&copybiases2lib:
        stackmasters4lib(biasessubs,'biases',optoffset,biaseslib,app,AO)

    if opt.ppdark&copydarks2lib:
        stackmasters4lib(darkssubs,'darks',optdark,darkslib,app,AO)

    if opt.ppflat&copyflats2lib:
        stackmasters4lib(flatssubs,'flats',optflat,flatslib,app,AO)

    
    if not(app.bOpened): # in case it was closed due to error
        app.Open()

    cmd=Wrapper(app)

    if needtochangeBDC: #restoring original bitdepth and compression settings
        print(_('Re-applying Siril original settings before lights processing'))
        cmd.Execute("setcompress "+origsettings.compression)
        cmd.Execute("set{:d}bits".format(origsettings.bitdepth))

    print('')
    for f in lightssubs:
        out=printout(_('Processing {0:s} from {1:s}').format('lights',f))
        workdir,last=os.path.split(f)
        opt.lights=last
        status,res=gessengine.Run(workdir,opt,app)
        if status:
            out=printout(_('Successful'))
        else:
            out=printout(res['message'])
            out=printout(_('{0:s} from {1:s} could not be processed - check the log').format('lights',f))

        out=printout('')


    return cleanclose(_('Finished'),out,app,True)




def main():

    args=[]
    kwargs={}
    for a in sys.argv[1:]:
        if '=' in a:
            f,v=a.split('=',1)
            kwargs[f]=v
        else:
            args.append(a)

    Run(*tuple(args),**kwargs)


if __name__ == "__main__":
    
    args=[]
    kwargs={}
    for a in sys.argv[1:]:
        if '=' in a:
            f,v=a.split('=',1)
            kwargs[f]=v
        else:
            args.append(a)

    Run(*tuple(args),**kwargs)