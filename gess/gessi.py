""" 
    This module is used to call interactively :py:mod:`gess.gessengine`. More help on its usage can be found in :ref:`interactive mode<interactive mode>` section.
    
    It takes no input and returns the (bool,dict) tuple output by gessengine.

"""
import os,sys
from distutils.util import strtobool


if __package__ is None or len(__package__)==0: # for debugging purposes only
    sys.path.append(".")

from gess import gessengine
from gess.common.options import options
from gess.common.helpers import returninifolder
from gess.common.i18n import _


def Run(addgesscfg='',addopt={},dryrun=False,fromgui=False):

    try:
        dryrun=strtobool(dryrun)
    except:
        pass

    try:
        fromgui=strtobool(fromgui)
    except:
        pass

    Options=options(addcfgfile=addgesscfg,dictcfg=addopt)
    opt=Options.getoptions()
    if len(opt)==0:
        print(_('Could not read the additional cfg file - aborting'))
        return False,{}
    # Reading the ini file to start the directory selection from last point

    inifile=os.path.join(Options.myhome,'gess.ini')
    workdir=returninifolder(inifile)
    if len(workdir)==0: # Cancel was pressed
        return False,{}


    print(_('gess default options from {:s}').format(Options.defcfgfile))
    if len(addgesscfg)>0:
        print(_('with additional options from {:s}').format(Options.addcfgfile)) 
    if len(addopt)>0:
        print(_('with additional options from arguments dictionnary'))


    for k,v in opt.items():
        print(k+' : '+str(v))
    print('')

    if not fromgui:
        Done=False
        print(_('You can change any of the options above'))
        print(_('Type the option name and updated value, separated by a = sign and press Enter'))
        print(_('If valid, the option value is updated'))
        print(_('Press Enter on an empty line when you are done'))
        while not Done:
            o = input('')
            if len(o)==0:
                Done=True
            else:
                k,v=o.split('=')
                k=k.strip()
                o=o.strip()
                if k in opt:
                    if isinstance(opt[k],bool):
                        try:
                            opt[k]=bool(strtobool(v))
                        except:
                            print(_('Could not convert your entry to boolean value'))
                    elif isinstance(opt[k],float):
                        try:
                            opt[k]=float(v)
                        except:
                            print(_('Could not convert your entry to float value'))
                    elif isinstance(opt[k],int):
                        try:
                            opt[k]=int(v)
                        except:
                            print(_('Could not convert your entry to integer value'))
                    else:
                        opt[k]=v
                else:
                    print(_('This option does not exists'))
    os.system('cls') # TODO: command is 'clear' under Unix systems
    return gessengine.Run(workdir,opt,dryrun=dryrun)


def main():

    args=[]
    kwargs={}
    for a in sys.argv[1:]:
        if '=' in a:
            f,v=a.split('=',1)
            kwargs[f]=v
        else:
            args.append(a)

    Run(*tuple(args),**kwargs)


if __name__ == "__main__":
    
    args=[]
    kwargs={}
    for a in sys.argv[1:]:
        if '=' in a:
            f,v=a.split('=',1)
            kwargs[f]=v
        else:
            args.append(a)

    Run(*tuple(args),**kwargs)