import PySimpleGUI as sg
import os,sys

if __package__ is None or len(__package__)==0: # for debugging purposes only
    sys.path.append(".")

from gess.common.options import options
from gess.common.DictX import DictX
from gess.common.i18n import _


def Run():

    sg.theme('Light Blue 3')

    msgdict={
        'folderin': _('Folder In: '),
        'folderout': _('Folder Out:'),
        'levelup': _('Level up:'),
        'prefix': _('Prefix:'),
        'suffix': _('Suffix:'),  
        'copyheader': _('Copy original FITS header'),    
        'useoriginalFITSnames': _('Use original FITS names'),
        'removepp': _('Remove calibrate prefix'),
        'onefolderperseq': _('Create one subfolder per sequence'),
        'searchprefix': _('Search prefix'),       
    }

    tooltipsdict={
        'folderin':_('String to be found in folders to be screened for calibrated files, default is \"process\"\n\ncopyback.cfg syntax:folderin=process'),
        'folderout':_('Path where to store the calibrated lights.\nThis string can contain FITS header keys and also path elements.\nExample: folderout=\'Calibrated/[FILTER:s]\'\n\ncopyback.cfg syntax:folderout=\"\"'),
        'levelup':_('Defines how many levels up folderin, folderout should be located.\nExample: with levelup=4, folderin=process and folderout=Calibrated/[FILTER:s]\nIf you have calibrated lights at /M31/21-01-2021/H-alpha/process32, then calibrated lights made with H-alpha filter will be copied to /M31/Calibrated/H-alpha\n\ncopyback.cfg syntax:level=1'), 
        'prefix':_('Prefix to be added in front of the calibrated lights names\nThis string can contain FITS header keys with format specifier, like prefix=[OBJECT:s]_\n\ncopyback.cfg syntax:prefix=\"\"'), 
        'suffix':_('Suffix to be added at the end of the calibrated lights names\nThis string can contain FITS header keys with format specifier, like suffix=_[GAIN:d]\n\ncopyback.cfg syntax:suffix=\"\"'), 
        'copyheader':_('If True, copies the FITS header of the original FITS file as it was written by your imaging software\nWarning: this requires to have astropy package installed!\n\ncopyback.cfg syntax:copyheader=false'), 
        'useoriginalFITSnames':_('If true, the copy keeps the original FITS filename as it was written by your imaging software (with prefix and suffix if any)\nIf false, it keeps the calibrated file names from Siril (with prefix and suffix if any).\n\n\ncopyback.cfg syntax:useoriginalFITSnames=true'), 
        'removepp':_('If true, the prefixes pp_ and the like will be removed from the calibrated files before copying\nIf false, it keeps the prefixes intact.\nThis setting is not used if you have chosen to use the original FITS names\n\n\ncopyback.cfg syntax:removepp=false'), 
        'onefolderperseq':_('If true, copyback creates one subfolder per sequence\nThis setting is not used if you have chosen to use the original FITS names\n\n\ncopyback.cfg syntax:onefolderperseq=true'),
        'searchprefix':_('Undocumented, leave as it is\n\n\n\ncopyback.cfg syntax:serachprefix=pp'),

    }

    warndict={
        'Clear': _('You are about to clear all data in this form.\nDo you want to proceed?'),
        'Update Default': _('You are about to save all these values as the new default options\nDo you want to proceed?'),
    }


    def _libinput(name,opt,msgdict,tooltipsdict):
        return [[   sg.Text(msgdict[name],size=(10,1)),
                    sg.Input(opt[name],key=name,tooltip=tooltipsdict[name],size=(50,1))]]

    def _folderinput(name,opt,msgdict,tooltipsdict):

        return [[   sg.Text(msgdict[name],size=(22,1)),
                    sg.Input(opt[name],key=name,tooltip=tooltipsdict[name],size=(70,1))]]

    def _setcheckbox(key,opt,msgdict,tooltipsdict):
        return [[ sg.Checkbox(msgdict[key],opt[key],key=key,tooltip=tooltipsdict[key]) ]]


    def updateoptions(wd,opt):
        updict={}
        for k in opt.keys():
            updict[k]=wd[k].Get()
        return updict

    def warningwd(warntext):
        return sg.popup_ok_cancel('Warning: '+warntext,title='Warning')



    Opt=options('copyback')
    opt=Opt.getoptions()


    copyback_layout = []
    copyback_layout+=[[sg.Text('GeSS Copyback options', font='Any 12')]]
    copyback_layout+=_libinput('folderin',opt,msgdict,tooltipsdict)
    copyback_layout+=_libinput('folderout',opt,msgdict,tooltipsdict)
    copyback_layout+=_libinput('levelup',opt,msgdict,tooltipsdict)
    copyback_layout+=_libinput('prefix',opt,msgdict,tooltipsdict)
    copyback_layout+=_libinput('suffix',opt,msgdict,tooltipsdict)
    copyback_layout+=_setcheckbox('copyheader',opt,msgdict,tooltipsdict)
    copyback_layout+=_setcheckbox('useoriginalFITSnames',opt,msgdict,tooltipsdict)
    copyback_layout+=_setcheckbox('removepp',opt,msgdict,tooltipsdict)
    copyback_layout+=_setcheckbox('onefolderperseq',opt,msgdict,tooltipsdict)
    copyback_layout+=_libinput('searchprefix',opt,msgdict,tooltipsdict)


    copyback_layout+=[[          
                sg.Button(_('Clear'),tooltip=_('Reset current options to default values')),
                    sg.Button(_('Load'),tooltip=_('Load an existing cfg file')),
                    sg.Button(_('Update Default'),tooltip=_('Update copyback default cfg file')),
                    sg.Button(_('Save As'),tooltip=_('Save as an optional cfg file')),
                    sg.Button(_('Print'),tooltip=_('Print options')),
                ]]
    window = sg.Window(_('GeSS copyback options wizard'), copyback_layout, default_element_size=(50,2),finalize=True)    



    while True: 

        event, values = window.read()  
        if event == sg.WIN_CLOSED:   
            break        # always,  always give a way out!    
               

        if event==_('Clear'):
            popup=warningwd(warndict['Clear'])
            if popup.lower()=='ok':
                Opt=options(optiontype='copyback',returnclean=True)
                opt=Opt.getoptions()
                for k in opt.keys():
                    window[k].Update(value=opt[k])


        if event==_('Load'):
            options.importcfgfile(Opt,update=False)
            opt=Opt.getoptions()
            for k in opt.keys():
                window[k].Update(value=opt[k])

        if event==_('Update Default'):
            popup=warningwd(warndict['Update Default'])
            if popup.lower()=='ok':
                Opt=options(optiontype='copyback',returnclean=True)
                opt=Opt.getoptions()
                newvals=updateoptions(window,opt)
                Opt=options(optiontype='copyback',dictcfg=newvals,updatedefault=True)

        if event==_('Save As'):
            Opt=options(optiontype='copyback',returnclean=True)
            opt=Opt.getoptions()
            newvals=updateoptions(window,opt)
            Opt=options(optiontype='copyback',dictcfg=newvals,updatedefault=False)
            Opt.exportcfgfile()
           

        if event==_('Print'):
            for k in opt.keys():
                if k in values.keys():
                    print(k+':'+str(values[k]))


            
    return True

if __name__ == "__main__":

    Run()