import sys
import os
import re
import gettext
import locale
from gess.common.setuplang import returninilanguage

# ==============================================================================
def resource_path(relative_path, pathabs=None):
    """ Get absolute path to resource, works for dev and for PyInstaller """
    try:
        # PyInstaller creates a temp folder and stores path in _MEIPASS
        base_path = getattr(sys, '_MEIPASS', os.path.abspath(os.path.split(os.path.dirname(__file__))[0]))
    except Exception:
        #base_path = os.path.abspath(".")
        if pathabs== None :
            base_path = os.path.dirname( sys.argv[0] )
        else:
            base_path = pathabs

    return os.path.join(base_path, relative_path)

try:

    language=returninilanguage()

    if not re.search( "_" ,language ) :
        language = language.lower() + "_" + language.upper()

                
    path_abs=os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    i18n_dir = resource_path( os.path.join('i18n' ,language),pathabs=path_abs )

    if not os.path.exists(i18n_dir )  :
        _ = gettext.gettext
    else:
        # lang = gettext.translation('gess', os.path.dirname(i18n_dir) , languages=[language,os.environ['LANG']] )
        lang = gettext.translation('gess', os.path.dirname(i18n_dir) , languages=[language] )
        _ = lang.gettext
except Exception as e :
    print("*** error init_language():" + str(e))
    _ = gettext.gettext
