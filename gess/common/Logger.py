import sys
from datetime import datetime
class Logger(object):
    """Class to log both to terminal and to .log file

    :param object: object
    :type object: object
    """
    def __init__(self,caller='gess_'):
        """Class constructor with default name

        :param caller: the suffix that will be added before the date-time the log was crated, defaults to 'gess_'
        :type caller: str, optional
        """
        self.terminal = sys.stdout
        self.dbgfile=caller+datetime.now().strftime('%Y%m%d_%H-%M-%S')+'.log'
        self.log = open(self.dbgfile, "w")
        

    def write(self, message):
        self.terminal.write(message)
        self.log.write(message)  

    def flush(self):
        pass