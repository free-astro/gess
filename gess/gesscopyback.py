""" 
    This module is used to copy calibrated lights around, most likely after having processed them with :py:mod:`gess.gessloop`. More help on its usage can be found in :ref:`copyback mode<copyback mode>` section.

"""

import os,sys
from astropy.io.fits.convenience import getheader
from pysiril.siril import Siril
from pysiril.addons import Addons
from datetime import datetime,timedelta
import glob,re
from pathlib import Path
from distutils.util import strtobool

if __package__ is None or len(__package__)==0: # for debugging purposes only
    sys.path.append(".")

from gess.common.i18n import _


from gess.common.options import options
from gess.common.DictX import DictX
from gess.common.Logger import Logger
from gess.common.helpers import fast_scandir,parsemasterformat,GetHeader

from gess.versions import __version__, __sirilminversion__
VERSION = __version__


def Run(addgesscfg='',addloopcfg='',searchstr='',addcopybackcfg=''):
    """ main function of gesscopyback.

    :param addgesscfg: the path to a gess-style additional cfg file, defaults to ''.
    :type addcfgfile: str, optional
    :param addloopcfg: the path to a loop-style additional cfg file, defaults to ''.
    :type addloopcfg: str, optional
    :param searchstr: the string to be searched for in your imaging folder, defaults to ''
    :type searchstr: str, optional
    :param addcopybackcfg: the path to a copyback-style additional cfg file, defaults to ''
    :type addcopybackcfg: str, optional

    :return: (True if successful, dict with 'log','in' and 'out' keys listing files which have been copied)
    :rtype: (bool,dict)
    """


    res={}
    loop=options(optiontype='loop',addcfgfile=addloopcfg).getoptions()
    opt=options(optiontype='gess',addcfgfile=addgesscfg).getoptions()
    cb=options(optiontype='copyback',addcfgfile=addcopybackcfg).getoptions()


    if opt.seqasfitseq:
        if cb.copyheader:
            print(_('Original headers cannot be copied if you use fitseq format - setting copyheader to false'))
            cb.copyheader=False
        if cb.useoriginalFITSnames:
            print(_('Original FITS filenames cannot be copied if you use fitseq format - setting useoriginalFITSnames to false'))
            cb.useoriginalFITSnames=False           

    if cb.levelup<=0:
        print(_('copyback levelup cannot be null or negative - aborting'))
        return False,res

    if cb.copyheader:
        try:
            from astropy.io import fits
        except:
            print(_('You need to have package astropy installed if you want to copy the original headers'))
            return False,res

    if cb.useoriginalFITSnames:
        if cb.removepp:
            print(_('No calibrate prefix to be removed if you ask to use original filenames'))
            cb.removepp=False

    maindir=loop.imagingfolder
    os.chdir(maindir)


    if opt.debug:
        sys.stdout = Logger('Copyback_')
        res['log']=os.path.join(maindir,sys.stdout.dbgfile)
        opt.debug=False

    print(_('Starting pySiril'))    
    if len(opt.sirilexe)==0:
        app=Siril(bStable=False,requires=__sirilminversion__)
    else:
        app=Siril(opt.sirilexe,bStable=False,requires=__sirilminversion__)
    print('')
    AO=Addons(app)
    app.tr.Configure(False,False,True,True) #Muting pySiril info and log 

    if len(searchstr)==0: # if no date is passed, then we look for the date 12 hrs before
        searchdate=datetime.now()-timedelta(hours=12)
        searchstr=searchdate.strftime(loop.datefmt)

    subbydate=[]

    print(_('Searching for all folders under {0:s} containing {1:s}').format(maindir,searchstr))

    ins=[]
    outs=[]
    
    for root, dirs, _dump in os.walk(maindir):
        for d in dirs:
            if searchstr in d:
                subbydate.append(os.path.join(root,d))

    if len(subbydate)==0:
        print(_('None found - aborting'))
        sys.exit()
    else:
        for s in subbydate:
            print(s)
        print('')


    folderins=[]
    for d in subbydate:
        allsubs=fast_scandir(d)
        print(_('Searching for all folders under {0:s} containing : {1:s}').format(d,cb.folderin))
        for a in allsubs:
            if (cb.folderin.lower() in os.path.split(a)[1].lower()):
                print(a)
                folderins.append(a)
    if len(folderins)==0:
        print(_('No {:s} folder found matching the date').format(cb.folderin))
    print('')    

    prefixes2remove=['pp_','ppd_','ppdf_','ppf_','ppdeb_','ppddeb_','ppdfdeb_','ppfdeb_']
    # repref='|'.join(prefixes2remove)

    if len(opt.lightsfmt)==0:
        lightstr=opt.lights
    else:
        lightstr=re.sub(r"[\[].*?[\]]", '.*', opt.lightsfmt)
    # pattern=('^pp.*_'+lightstr+'_.*\.(fit|fits|fts)')
    searchprefix=cb.searchprefix
    if not('pp') in searchprefix:
        for i,p in enumerate(prefixes2remove):
            prefixes2remove[i]=searchprefix+p
    pattern=('^'+searchprefix+'.*'+lightstr+'.*\.(fit|fits|fts)')           

    def glob_re(pattern, strings):
        return list(filter(re.compile(pattern,re.IGNORECASE).match, strings))
  

    for f in folderins:
        parentfolder = Path(f).parents[cb.levelup-1]

        filenames = glob_re(pattern, os.listdir(f))
        for ff in filenames:
            calfilename=os.path.join(f,ff)
            ffo=ff[:]
            for p in prefixes2remove:
                 ffo=ffo.replace(p,'')
            origfilename=os.path.join(f,ffo)
            sourcefilename=os.path.realpath(origfilename)
            hdrs=GetHeader(calfilename)
            hdr=GetHeader(sourcefilename)

            folderoutchecked=parsemasterformat(cb.folderout,hdr,sourcefilename,'w',rmspace=False)
            if cb.onefolderperseq:
                fff,_dump=os.path.splitext(ff)
                try:
                    fff,_dump=fff.rsplit('_',1)
                    _dump=int(_dump)
                except:
                    pass
                folderoutchecked=os.path.join(folderoutchecked,fff)
            targetfolder=parentfolder.joinpath(folderoutchecked)

            efs=[]
            maxnum=0
            if not targetfolder.exists():
                AO.MkDirs(targetfolder)

            else:
                existingfiles=glob_re('.*\.(fit|fits|fts)', os.listdir(targetfolder))
                for ef in existingfiles:
                    efi=DictX()
                    efi.name,efi.ext=os.path.splitext(ef)
                    efi.hdr=getheader(os.path.join(targetfolder,ef))
                    try:
                        efi.d=efi.hdr['DATE-OBS']
                    except:
                        efi.d='0' #case if original header was copied and DATE-OBS is not a key
                    try:
                        efi.num=int(efi.name.rsplit('_',1)[1]) # case if original filename or suffix
                    except:
                        efi.num=0

                    efs.append(efi)
                nums=[h.num for h in efs]
                if len(nums)>0:
                    maxnum=max(nums)
                    
            ds=[h.d for h in efs]
            if hdrs['DATE-OBS'] in ds: # the file was already copied back previously - avoiding duplicate
                print(_('File {0:s} (or a fits with same obs date) was already copied to folder {1:s} - Skipping').format(ff,str(targetfolder)))
                continue


            prefixchecked=parsemasterformat(cb.prefix,hdr,ff,'w') 
            suffixchecked= parsemasterformat(cb.suffix,hdr,ff,'w') 

            if not(cb.useoriginalFITSnames):
                if cb.removepp:
                    _dump,newname=os.path.split(origfilename) 
                else:
                    _dump,newname=os.path.split(calfilename)
                newname,ext=os.path.splitext(newname) 
                if not(opt.seqasfitseq):
                    newname,seqnum=newname.rsplit('_',1)
                    seqnum=int(seqnum)
                else:
                    seqnum=1
                if maxnum>0: # will need to renumber
                    seqnum=maxnum+1
                newname='{0:s}_{1:05d}'.format(newname,seqnum)
            else:
                _dump,newname=os.path.split(sourcefilename)
                newname,ext=os.path.splitext(newname) 
                
            newname=prefixchecked+newname+suffixchecked+ext
            dst=os.path.normpath(targetfolder.joinpath(newname))

            if cb.copyheader:
                with fits.open(sourcefilename) as hdulo:
                    hdro = hdulo[0].header  # the primary HDU header
                with fits.open(calfilename, mode='update') as hdul:
                    hdul[0].header=hdro
                    hdul.writeto(dst,overwrite=True)
                print(_('Copied file and original header from {0:s} to {1:s}').format(calfilename,dst))
            else:
                AO.CopyLink(calfilename,dst,bCopyMode=True)
                print(_('Copied file from {0:s} to {1:s}').format(calfilename,dst))
            ins.append(calfilename)
            outs.append(dst)
                

    res['in']=ins
    res['out']=outs
    return True,res


if __name__ == "__main__":
    
    args=[]
    kwargs={}
    for a in sys.argv[1:]:
        if '=' in a:
            f,v=a.split('=',1)
            kwargs[f]=v
        else:
            args.append(a)

    Run(*tuple(args),**kwargs)