""" 
    This module is used to gather calibrated lights from multiple sessions, already processed with :py:mod:`gess.gessengine`. More help on its usage can be found in :ref:`multisession mode<multisession mode>` section.
    
    It can take as input an additional gess-type cfg file (if you have used one for the lights calibration) and returns the (bool,dict) tuple output by gessengine.

"""
import os,sys
import tkinter as tk
from tkinter import filedialog
import glob
from pysiril.siril import Siril
from pysiril.addons import Addons

#TODO check removal of trailing underscore which do not exist for fitseq

if __package__ is None or len(__package__)==0: # for debugging purposes only
    sys.path.append(".")

from gess import gessengine

from gess.common.helpers import GetHeader, returninifolder,masterstackingoptions
from gess.common.options import options
from gess.common.i18n import _

from gess.versions import __version__, __sirilminversion__
VERSION = __version__


def Run(addcfgfile=''):
    """[summary]

    :param addcfgfile: the path to a gess-style additional cfg file, if one was used to calibrate your lights, defaults to ''.
    :type addcfgfile: str, optional

    :return: (True if successful, dict with keys specifying inputs and outputs)
    :rtype: (bool,dict)

    see :py:mod:`gess.gessengine` for details on returned values.
    """  
    
    Options=options(addcfgfile=addcfgfile)
    opt=Options.getoptions()
    # print('Starting pySiril')    
    if len(opt.sirilexe)==0:
        app=Siril(bStable=False,requires=__sirilminversion__)
    else:
        app=Siril(opt.sirilexe,bStable=False,requires=__sirilminversion__)
    AO=Addons(app)  
    inifile=os.path.join(Options.myhome,'gess.ini')

    Done=False
    mergefiles=[]
    mergefiles2=[]
    First=True
    multipath=[]
    while not Done:
        if First:
            adddir=returninifolder(inifile,_('Select folders containing calibrated lights - Cancel when done'))

        else:
            parent=tk.Tk()
            parent.withdraw()
            adddir = filedialog.askdirectory(initialdir=adddir,title=_('Select folders containing calibrated lights - Cancel when done'))
            parent.destroy()
            
        if len(adddir)==0:
            if First:
                print(_('Cancelled by the user'))
                res={}
                res['message']=_('Cancelled by the user')
                return False,res
            else:
                Done=True
        else:
            adddir=os.path.normpath(adddir)
            multipath.append(os.path.split(adddir)[0])
            if First:
                # find the file extension
                for f in glob.iglob(os.path.join(adddir,'*pp*.f*')):
                    opt.ext=os.path.splitext(f)[1].replace('.','')
                    break

                files = glob.glob(os.path.join(adddir,'*pp*.'+opt.ext))
                if len(files)==0:
                    files = glob.glob(os.path.join(adddir,opt.lights+'.'+opt.ext)) # special case of mono w/o any calibration
                files=[f.rsplit('_',1)[0] for f in files]
                basenames=set(files)
                basenames=[os.path.split(b)[1]+'_' for b in basenames] #removing path
                basenames=[x for x in basenames if not x.startswith('r_')] # removing registered sets
                # v0.4.4
                basenames=[x for x in basenames if not 'dark' in x.lower()] # removing flats 
                basenames=[x for x in basenames if not 'flat' in x.lower()] # removing darks 
                basenames=[x for x in basenames if not 'oiii' in x.lower()] # removing OIII
                if len(opt.biases)>0:
                    basenames=[x for x in basenames if not opt.biases.lower() in x.lower()] # removing biases
                # end v0.4.4                                           
                basenames.sort()

                print(_('Found {:d} sets of files - Type the number of the set you want to use:').format(len(basenames)))
                for i,b in enumerate(basenames):
                    print('[{0:d}] : '.format(i+1)+b)
                n = input('')
                setnumberisvalid=False
                while not setnumberisvalid:
                    try:
                        basename=basenames[int(n)-1]
                        setnumberisvalid=True
                    except:
                        print(_('Set number is not valid - retry'))
                        n = input('')
                
                First=False
                basename2=''
                if 'Ha' in basename: # we need to look for OIII as well
                    # if any(['OIII' in b for b in basenames]):
                    basename2=basename.replace('Ha','OIII')
                # if 'OIII' in basename: # we need to look for Ha as well
                #     if any(['Ha' in b for b in basenames]):
                #         basename2=basename.replace('OIII','Ha')


            files = glob.glob( os.path.join(adddir,basename+'*.'+opt.ext))
            if len(basename2)>0:
                files2= glob.glob( os.path.join(adddir,basename2+'*.'+opt.ext))
                opt.doHO=True
            else:
                files2=[]
                opt.doHO=False
            if len(files2)==0:
                print(_('Found {:d} files').format(len(files)))
            else:
                print(_('Found {0:d} + {1:d} files').format(len(files),len(files2)))
            for f in files: mergefiles.append(f)
            for f in files2: mergefiles2.append(f)
        if len(adddir)>0:
            adddir=os.path.normpath(os.path.join(adddir,'../'))

    # removing last underscore
    if 'OIII' in basename: #need to make sure opt.lights contains 'Ha' (if HO)
        opt.lights=basename2[:-1] 
    else:
        opt.lights=basename[:-1] 

    
    choices={'0':_('Cancel'),
             '1':_('Gather files from multiple sessions'),
             '2':_('Gather files and register'), 
             '3':_('Gather files, register and stack'),
             }
    print(_('What do you want to do next ? Type the number of one of the options below'))
    for k,i in choices.items():
        print('['+k+'] : '+i)

    n = input('')    
    if n=='0':
        print(_('Cancelled by the user'))
        res={}
        res['message']=_('Cancelled by the user')
        return False,res

    setnumberisvalid=False
    while not setnumberisvalid:
        try:
            _dump=choices[n]
            setnumberisvalid=True
        except:
            print(_('Option number is not valid - retry'))
            n = input('')

    for k in opt.keys():
        if k.startswith('pp'):
            opt[k]=False

    
    if n=='1':
        opt.doregister=False
        opt.dostack=False
    if n=='2':
        opt.doregister=True
        opt.dostack=False  
    if n=='3':
        opt.doregister=True
        opt.dostack=True  



    # find the bitdepth through fits header
    hdr=GetHeader(mergefiles[0])

    if 'BITPIX' in hdr.keys():
        opt.bitdepth=abs(int(hdr['BITPIX']))
    if 'ZBITPIX' in hdr.keys(): #compressed data - must be called after as BITPIX is also a header key
        opt.bitdepth=abs(int(hdr['ZBITPIX']))

    # otherwise, we leave it as it is in the cfg file    

    # options are set, we can copy the files   

    workdirselected=False
    while not workdirselected:
        parent=tk.Tk()
        parent.withdraw()
        workdir = filedialog.askdirectory(initialdir=adddir,title=_('Select/create folder to store the multisession merge'))
        parent.destroy()

        if len(workdir)==0: # Cancel was pressed
            print(_('Cancelled by the user'))
            res={}
            res['message']=_('Cancelled by the user')
            return False,res
        else:
            workdir = os.path.realpath(workdir)
            if not any([workdir == p for p in multipath]): # avoiding to select one of the original process folders
                workdirselected=True
            else:
                print(_('You cannot choose one of the original process folders to store the multisession merge'))
                print(_('Select another location'))

          
    print(_('Gathering {:d} files').format(len(mergefiles)))

    copydir=os.path.join(workdir,'process'+str(opt.bitdepth))
    AO.MkDirs(copydir)
    filesready=AO.NumberImages(mergefiles, copydir, basename, start=1, 
                     extDest='.'+opt.ext, bCopyMode=True, bOverwrite = False , preserveorder=True)
    filesready2=0
    if len(mergefiles2)>0:
        if not len(mergefiles2)==len(mergefiles):
            print(_('Did not find same number of Ha and OIII images - aborting'))
            res={}
            res['message']=_('Did not find same number of Ha and OIII images - aborting')
            return False,res
        else:
            filesready2=AO.NumberImages(mergefiles2,copydir, basename2, start=1, 
                     extDest='.'+opt.ext, bCopyMode=True, bOverwrite = False , preserveorder=True)        
    

    if not(filesready==len(mergefiles)):
        print(_('There was a problem when copying the {:s} files - check the log').format(basename))
        res={}
        res['message']=_('There was a problem when copying the {:s} files').format(basename)
        return False,res
    
    if not(filesready2==len(mergefiles2)):
        print(_('There was a problem when copying the {:s} files - check the log').format(basename2))
        res={}
        res['message']=_('There was a problem when copying the {:s} files').format(basename) # JEP: Shouldn't it be basename2
        return False,res

    if opt.doregister:
        # Running gess engine if we need to at least register
        success,res=gessengine.Run(workdir,opt,app)
        app.Close()
        del app
        return success,res
    else:
        res={}
        res['message']=_('gess multisession run completed')
        del app
        return True,res




if __name__ == "__main__":

    args=[]
    kwargs={}
    for a in sys.argv[1:]:
        if '=' in a:
            f,v=a.split('=',1)
            kwargs[f]=v
        else:
            args.append(a)

    Run(*tuple(args),**kwargs)